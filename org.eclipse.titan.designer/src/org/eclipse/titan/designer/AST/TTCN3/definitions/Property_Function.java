/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import java.util.List;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.ILocateableNode;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Base class for class property getters and setters
 * 
 * @author Miklos Magyari
 *
 */
public abstract class Property_Function extends Scope implements ILocateableNode {
	private Location location;
	private Location modifierLocation;
	private Location visibilityLocation;
	protected CompilationTimeStamp lastTimeChecked;

	protected boolean isAbstract;
	protected boolean isFinal;
	protected boolean isDeterministic;
	private VisibilityModifier visibility;
	protected Definition myDef;

	public Property_Function(boolean isAbstract, boolean isFinal, boolean isDeterministic) {
		this.isAbstract = isAbstract;
		this.isFinal = isFinal;
		this.isDeterministic = isDeterministic;
	}
	
	@Override
	public Assignment getAssBySRef(CompilationTimeStamp timestamp, Reference reference) {
		return parentScope.getAssBySRef(timestamp, reference);
	}
	
	@Override
	public Assignment getAssBySRef(CompilationTimeStamp timestamp, Reference reference,
			IReferenceChain referenceChain) {
		return parentScope.getAssBySRef(timestamp, reference, referenceChain);
	}
	
	public void check(CompilationTimeStamp timestamp, Definition definition) {
		myDef = definition;
		parentScope = myDef.getMyScope();
		if (visibility == VisibilityModifier.Friend) {
			visibility = myDef.getVisibility();
		}
	}

	public Definition getMyDefinition() {
		return myDef;
	}
	
	public void setModifierLocation(Location location) {
		modifierLocation = location;
	}
	
	public Location getModifierLocation() {
		return modifierLocation;
	}

	@Override
	public Assignment getEnclosingAssignment(int offset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findReferences(ReferenceFinder referenceFinder, List<Hit> foundIdentifiers) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	public VisibilityModifier getVisibility() {
		return visibility;
	}

	public void setVisibility(VisibilityModifier visibility) {
		this.visibility = visibility;
	}

	public Location getVisibilityLocation() {
		return visibilityLocation;
	}

	public void setVisibilityLocation(Location visibilityLocation) {
		this.visibilityLocation = visibilityLocation;
	}
}
