/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.statements;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.designer.AST.ASTNode;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.TTCN3.IIncrementallyUpdateable;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * 
 * @author Miklos Magyari
 *
 */
public class SelectClassCases extends ASTNode implements IIncrementallyUpdateable {
	private final List<SelectClassCase> select_class_cases;
	
	public SelectClassCases() {
		select_class_cases = new ArrayList<SelectClassCase>();
	}
	
	public void addSelectClassCase(final SelectClassCase selectCase) {
		synchronized(select_class_cases) {
			select_class_cases.add(selectCase);
		}
		selectCase.setFullNameParent(this);
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		for (final SelectClassCase select_class_case : select_class_cases) {
			select_class_case.setMyScope(scope);
		}
	}
	
	public void setMyStatementBlock(final StatementBlock statementBlock, final int index) {
		for (int i = 0, size = select_class_cases.size(); i < size; i++) {
			select_class_cases.get(i).setMyStatementBlock(statementBlock, index);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (select_class_cases == null) {
			return;
		}

		for (final SelectClassCase sc : select_class_cases) {
			sc.findReferences(referenceFinder, foundIdentifiers);
		}
	}
	
	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		for (final SelectClassCase sc : select_class_cases) {
			sc.updateSyntax(reparser, false);
			reparser.updateLocation(sc.getLocation());
		}
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (select_class_cases != null) {
			for (final SelectClassCase sc : select_class_cases) {
				if (!sc.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}

	public void check(final CompilationTimeStamp timestamp, final Class_Type refClass) {
		boolean unrechable = false;
		for (final SelectClassCase sc: select_class_cases) {
			unrechable = sc.check(timestamp, refClass, unrechable);
		}
	}
	
	public void postCheck() {
		for (final SelectClassCase sc: select_class_cases) {
			sc.postCheck();
		}
	}
}
