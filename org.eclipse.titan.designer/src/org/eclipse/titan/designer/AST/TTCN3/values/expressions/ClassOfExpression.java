/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.values.expressions;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.values.Boolean_Value;
import org.eclipse.titan.designer.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a class of-operator 
 * ETSI ES 203 790 V1.3.1 (2021-05) 
 * 5.1.2.5 
 * 
 * @author Miklos Magyari
 *
 */
public class ClassOfExpression extends Expression_Value {
	private final Reference objectReference;
	private final Type ofType;
	
	public ClassOfExpression(final Reference leftReference, final Type rightReference) {
		this.objectReference = leftReference;
		this.ofType = rightReference;
		
		if (leftReference != null) {
			leftReference.setFullNameParent(this);
		}
		
		if (rightReference != null) {
			rightReference.setFullNameParent(this);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (objectReference != null) {
			objectReference.setMyScope(scope);
		}
		if (ofType != null) {
			ofType.setMyScope(scope);
		}
	}
	
	@Override
	public Operation_type getOperationType() {
		return Operation_type.CLASS_OF_OPERATION;
	}

	@Override
	public IValue evaluateValue(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}
		// FIXME :: implement : we should return the real value
		lastValue = new Boolean_Value(true);
		return lastValue;
	}

	@Override
	public Type_type getExpressionReturntype(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		return Type_type.TYPE_BOOL;
	}
	
	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isUnfoldable(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String createStringRepresentation() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected boolean memberAccept(ASTVisitor v) {
		// TODO Auto-generated method stub
		return false;
	}
}
