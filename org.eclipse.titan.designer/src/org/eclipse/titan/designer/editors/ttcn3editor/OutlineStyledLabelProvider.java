package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.IOutlineElement;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.NamedBridgeScope;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.designer.editors.AstSyntaxHighlightTokens;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.PlatformUI;

/**
 * This class represents the outline label provider that supports StyledString
 * 
 * @author Miklos Magyari
 *
 */
public final class OutlineStyledLabelProvider extends DelegatingStyledCellLabelProvider {

	@Override
	protected void erase(final Event event, final Object element) {
		if ((event.detail & SWT.SELECTED) != 0) {
			event.detail &= ~SWT.SELECTED;

			Rectangle bounds = event.getBounds();
			final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			final RGB rgb = StringConverter.asRGB(
				store.getString(PreferenceConstants.COLOR_PREFIX + PreferenceConstants.STYLED_SELECTION_INACTIVE_BACKGROUND), null);		
			
			final Color bgColor = new Color(PlatformUI.getWorkbench().getDisplay(), rgb);
			event.gc.setBackground(bgColor);
			event.gc.fillRectangle(bounds);
		}

		super.erase(event, element);
	}

	@Override
	protected void measure(final Event event, final Object element) {
		event.detail &= ~SWT.SELECTED;
		super.measure(event, element);
	}

	@Override
	protected void paint(final Event event, final Object element) {
		event.detail &= ~SWT.SELECTED;
		super.paint(event, element);
	}

	public OutlineStyledLabelProvider() {
		super(new IStyledLabelProvider() {

			@Override
			public Image getImage(Object element) {
				String iconName = "titan.gif";
				if (element instanceof IOutlineElement) {
					final IOutlineElement e = (IOutlineElement) element;
					iconName = e.getOutlineIcon();
				} else if (element instanceof List<?>) {
					iconName = "imports.gif";
				}
				
				return ImageCache.getImage(iconName);
			}
			
			@Override
			public void addListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
			}

			@Override
			public void dispose() {
				// TODO Auto-generated method stub
			}

			@Override
			public boolean isLabelProperty(Object element, String property) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void removeListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
			}

			@Override
			public StyledString getStyledText(Object element) {
				Identifier identifier = null;
				if (element instanceof IOutlineElement) {
					final IOutlineElement e = (IOutlineElement) element;
					final String outlineText = e.getOutlineText();
					if (outlineText.length() != 0) {
						return getStyledText(element, outlineText);
					}
					identifier = e.getIdentifier();
				} else if (element instanceof List<?>) {
					return new StyledString("imports");
				}

				if (identifier == null) {
					return new StyledString("unknown");
				}
				return getStyledText(element, identifier.getDisplayName());
			}
			
			/**
			 * @param element
			 * @param text
			 * @return
			 */
			private StyledString getStyledText(Object element, String text) {
				final StyledString styledOutline = new StyledString();
				final IPreferencesService prefs = Platform.getPreferencesService();
				IToken token = null;
				String category = null;
				if (element instanceof Definition) {
					final Definition elemDef = (Definition)element;
					if (elemDef instanceof ICommentable) {
						ICommentable commentable = (ICommentable)elemDef;
						if (commentable.hasDocumentComment() && commentable.getDocumentComment().hasCategory()) {
							category = commentable.getDocumentComment().getCategory(); 
						}
					}
					final Location idloc = elemDef.getIdentifier().getLocation();
					final boolean isSemanticHighlightingEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
							PreferenceConstants.OUTLINE_ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
					if (isSemanticHighlightingEnabled) {
						token  = AstSyntaxHighlightTokens.getSyntaxToken(idloc);						
					}
					
					if (elemDef.getMyScope().isClassScope()) {
						Stylers.ColoredStyler styler = null;
						switch (elemDef.getVisibilityModifier()) {
						case Private:
							styler = new Stylers.ColoredStyler(Stylers.PrivateColor);
							break;
						case Public:
							styler = new Stylers.ColoredStyler(Stylers.PublicColor);
							break;
						default:
							styler = new Stylers.ColoredStyler(Stylers.ProtectedColor);
						}
						styledOutline.append(text);
						styledOutline.append(" \u25fc", styler);
					} 
				} 
				if (styledOutline.length() == 0) {
					if (token != null) {
						styledOutline.append(text, new Stylers.TextAttributeStyler((TextAttribute)token.getData()));
					} else {
						styledOutline.append(text);
					}
				}
				if (category != null && category.length() > 0) {
					final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
					final RGB categoryColor = StringConverter.asRGB(
							store.getString(PreferenceConstants.COLOR_PREFIX + PreferenceConstants.OUTLINE_CATEGORY_COLOR), null);
					styledOutline.append(" [")
						.append(category, new Stylers.TextAttributeStyler(
								new TextAttribute(new Color(Display.getCurrent(), categoryColor))))
						.append(']');
				}
				return styledOutline;			
			}			
		});
	}
}
